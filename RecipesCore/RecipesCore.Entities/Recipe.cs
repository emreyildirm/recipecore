﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecipesCore.Entities
{
    public class Recipe
    {
        public int RecipeId { get; set; }
        public string RecipeExplanation { get; set; }
        public IEnumerable<Ingredient> Ingredients { get; set; }
    }
}
