﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecipesCore.Entities
{
    public class Ingredient
    {
        public int IngredientId { get; set; }
        public string IngredientName { get; set; }
    }
}
